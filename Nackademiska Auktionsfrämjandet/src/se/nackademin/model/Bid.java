package se.nackademin.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Bid {
	private IntegerProperty sum;
	private StringProperty auctionNr;
	private StringProperty date;
	private IntegerProperty customerNr;
	
	public Bid(int sum, int customerNr, String date, String auctionNr){
		this.sum = new SimpleIntegerProperty(sum);
		this.auctionNr = new SimpleStringProperty(auctionNr);
		this.date = new SimpleStringProperty(date);
		this.customerNr = new SimpleIntegerProperty(customerNr);
	}
	
	public IntegerProperty getCustomerNr() {
		return customerNr;
	}
	public IntegerProperty getSum() {
		return sum;
	}
	public StringProperty getAuctionNr() {
		return auctionNr;
	}
	public StringProperty getDate() {
		return date;
	}
}