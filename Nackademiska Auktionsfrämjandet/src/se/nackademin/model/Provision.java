package se.nackademin.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Provision {
	private IntegerProperty salesTotal;
	private IntegerProperty provisionTotal;
	private StringProperty month;
	private StringProperty year;

	public Provision(int salesTotal, int provisionTotal,
			String month, String year) {
		super();
		this.salesTotal = new SimpleIntegerProperty(salesTotal);
		this.provisionTotal = new SimpleIntegerProperty(provisionTotal);
		this.month = new SimpleStringProperty(month);
		this.year = new SimpleStringProperty(year);
	}
	public IntegerProperty getSalesTotal() {
		return salesTotal;
	}
	public IntegerProperty getProvisionTotal() {
		return provisionTotal;
	}
	public StringProperty getMonth() {
		return month;
	}
	public StringProperty getYear() {
		return year;
	}

}
