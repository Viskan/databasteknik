package se.nackademin.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Customer {
	
	private IntegerProperty customerNr;
	private StringProperty name;
	private StringProperty address;
	private IntegerProperty zipCode; 
	private StringProperty area;
	private StringProperty phoneNr;
	private StringProperty mail;
	private IntegerProperty provisionLevel;
	private IntegerProperty total;

	public Customer(int customerNr, String name, String address, int zipCode, String area,
			String phoneNr, String mail, Integer provisionLevel) {
		this.customerNr = new SimpleIntegerProperty(customerNr);
		this.name = new SimpleStringProperty(name);
		this.address = new SimpleStringProperty(address);
		this.zipCode = new SimpleIntegerProperty(zipCode);
		this.area = new SimpleStringProperty(area);
		this.phoneNr = new SimpleStringProperty(phoneNr);
		this.mail = new SimpleStringProperty(mail);
		if(provisionLevel == null)
			this.provisionLevel = null;
		else
			this.provisionLevel = new SimpleIntegerProperty(provisionLevel);
	}

	public Customer(int kundNr, String namn, String adress, int postNr, String ort,
			String telefonNr, String email, Integer provisionsnivå, int total) {
		this.customerNr = new SimpleIntegerProperty(kundNr);
		this.name = new SimpleStringProperty(namn);
		this.address = new SimpleStringProperty(adress);
		this.zipCode = new SimpleIntegerProperty(postNr);
		this.area = new SimpleStringProperty(ort);
		this.phoneNr = new SimpleStringProperty(telefonNr);
		this.mail = new SimpleStringProperty(email);
		if(provisionsnivå == null)
			this.provisionLevel = null;
		else
			this.provisionLevel = new SimpleIntegerProperty(provisionsnivå);
		this.total = new SimpleIntegerProperty(total);
	}

	public IntegerProperty getCustomerNr(){
		return customerNr;
	}
	public StringProperty getName(){
		return name;
	}
	public StringProperty getAddress(){
		return address;
	}
	public IntegerProperty getZipCode(){
		return zipCode;
	}
	public StringProperty getArea(){
		return area;
	}
	public StringProperty getPhoneNr(){
		return phoneNr;
	}
	public StringProperty getMail(){
		return mail;
	}
	public IntegerProperty getProvisionLevel() {
		return provisionLevel;
	}
	public IntegerProperty getTotal() {
		return total;
	}
}

