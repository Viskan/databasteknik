package se.nackademin.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Auction {
	
	private IntegerProperty auctionNr;
	private StringProperty category;
	private StringProperty supplier;
	private StringProperty name;
	private StringProperty description;
	private IntegerProperty startingPrice;
	private IntegerProperty buyOut;
	private StringProperty startDate;
	private StringProperty endDate;
	private StringProperty ongoing;
	
	public Auction(int auctionNr, String category, String supplier,
			String name, String description, int startingPrice, int buyOut,
			String startDate, String endDate, boolean ongoing) {
		
		this.auctionNr = new SimpleIntegerProperty(auctionNr);
		this.category = new SimpleStringProperty(category);
		this.supplier = new SimpleStringProperty(supplier);
		this.name = new SimpleStringProperty (name);
		this.description = new SimpleStringProperty(description);
		this.startingPrice = new SimpleIntegerProperty(startingPrice);
		this.buyOut = new SimpleIntegerProperty(buyOut);
		this.startDate = new SimpleStringProperty(startDate);
		this.endDate = new SimpleStringProperty(endDate);
		if(ongoing)
			this.ongoing = new SimpleStringProperty("P�g�ende");
		else
			this.ongoing = new SimpleStringProperty("Ej P�g�ende");
		
	}
	public IntegerProperty getAuctionNr() {
		return auctionNr;
	}
	public StringProperty getCategory() {
		return category;
	}
	public StringProperty getSupplier() {
		return supplier;
	}
	public StringProperty getName() {
		return name;
	}
	public StringProperty getDescription() {
		return description;
	}
	public IntegerProperty getStartingPrice() {
		return startingPrice;
	}
	public IntegerProperty getBuyOut() {
		return buyOut;
	}
	public StringProperty getStartDate() {
		return startDate;
	}
	public StringProperty getEndDate() {
		return endDate;
	}
	public StringProperty getOngoing() {
		return ongoing;
	}
}