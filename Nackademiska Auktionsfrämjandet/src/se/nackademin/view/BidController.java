package se.nackademin.view;

import se.nackademin.model.Auction;
import se.nackademin.model.Bid;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class BidController {

	private Auction auction;
	private int auctionNr;
	private DatabaseController ac;
	private Stage dialogStage;
	private ObservableList<Bid> bidList = FXCollections.observableArrayList();

	@FXML private TableView<Bid> bidTable;
	@FXML private TableColumn<Bid, Number> customerNr;
	@FXML private TableColumn<Bid, Number> sum;
	@FXML private TableColumn<Bid, String> date;
	
	@FXML private TextField newBid;
	@FXML private TextField customerNrBid;
	@FXML private Button btnBid;
	@FXML private Label auctionStatus;
	@FXML private Label auctionProvision;
	
	@FXML
	private void initialize() {
	}
	
	public void setAuctionNr(Auction auction, DatabaseController ac2){
		this.ac = ac2;
		this.auction = auction;
		auctionNr = auction.getAuctionNr().get();
		int provision = ac.getAuctionProvision(auctionNr);
		
		System.out.println(provision);
		
		
		
		if(auction.getOngoing().getValue().equalsIgnoreCase("ej p�g�ende")){
			btnBid.setDisable(true);
			auctionStatus.setText("Auktion P�g�r Ej");
			auctionStatus.setTextFill(Color.RED);
			auctionProvision.setText("Provision: " + provision + " SEK"); 
		}
		else{
			auctionStatus.setText("Auktion P�g�r");
			auctionStatus.setTextFill(Color.GREEN);
		}
		
		setBidTable();
	}
	
	private void setBidTable(){
		bidList = ac.getBidList(auctionNr);
		
		customerNr.setCellValueFactory(cellData -> cellData.getValue().getCustomerNr());
		sum.setCellValueFactory(cellData -> cellData.getValue().getSum());
		date.setCellValueFactory(cellData -> cellData.getValue().getDate());
		
		bidTable.setItems(bidList);
	}

	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}

	public void handleClose(){
		dialogStage.close();
	}
	
	public void addNewBid(){
		ac.updateBid(customerNrBid.getText(), Integer.parseInt(newBid.getText()), auction.getAuctionNr().get());
		customerNrBid.clear();
		newBid.clear();
		
		setBidTable();
	}
}
