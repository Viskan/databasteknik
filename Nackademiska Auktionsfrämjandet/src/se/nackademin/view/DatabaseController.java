package se.nackademin.view;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;

import se.nackademin.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DatabaseController {
	private static final String USERNAME = "root";
	private static final String PASSWORD = "";
	private static final String CON = "jdbc:mysql://localhost/auctions";
	Connection conn;
	AuctionController ac;

	public DatabaseController(AuctionController ac){
		this.ac = ac;
		try {
			conn = DriverManager.getConnection(CON, USERNAME, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ObservableList<String> getCategory(){
		ObservableList<String> categoryList = FXCollections.observableArrayList();
		try(
				Statement stm = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
				ResultSet rs = stm.executeQuery("SELECT Kategori from Kategori");
				) {
			while(rs.next()){
				categoryList.add(rs.getString("Kategori"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return categoryList;
	}

	public ObservableList<Customer> getCostumerList(){
		ObservableList<Customer> customerList = FXCollections.observableArrayList();

		try {
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from kund left join leverantör on kund.kundnr = leverantör.kundnr");
			while(rs.next()){
				customerList.add(new Customer(
						rs.getInt("kundNr"),
						rs.getString("namn"),
						rs.getString("adress"),
						rs.getInt("postNr"),
						rs.getString("ort"),
						rs.getString("telefonNr"),
						rs.getString("email"),
						rs.getInt("provisionsnivå")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
		return customerList;
	}

	public ObservableList<Auction> getAuctionList(String query){
		ObservableList<Auction> auctionList = FXCollections.observableArrayList();
		try {
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(query);
			while(rs.next()){
				auctionList.add(new Auction(
						rs.getInt("auktionsNr"),
						rs.getString("kategori"),
						rs.getString("leverantör"),
						rs.getString("namn"),
						rs.getString("beskrivning"),
						rs.getInt("utgångspris"),
						rs.getInt("acceptpris"),
						rs.getString("startdatum"),
						rs.getString("slutdatum"),
						rs.getBoolean("pågående")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
		return auctionList;
	}

	public ObservableList<Customer> getCustomerListTotal(){
		ObservableList<Customer> customerList = FXCollections.observableArrayList();
		try {
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select kund.*, sum(summa) as total from bud "
					+ "inner join kund on kund.kundNr = bud.kund "
					+ "inner join auktion on auktion.auktionsNr = bud.auktion "
					+ "where kund.kundNr = bud.kund and summa >= all (select max(bud.summa) from bud where bud.auktion = auktion.auktionsNr and auktion.pågående = false) "
					+ "group by kund.kundnr "
					+ "order by total desc;");
			while(rs.next()){
				customerList.add(new Customer(
						rs.getInt("kundNr"),
						rs.getString("namn"),
						rs.getString("adress"),
						rs.getInt("postNr"),
						rs.getString("ort"),
						rs.getString("telefonNr"),
						rs.getString("email"),
						null,
						rs.getInt("total")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
		return customerList;
	}

	public ObservableList<Customer> addCustomer(String namn, String adress, int postNr, String ort, String telefonNr, String email){
		try(
				PreparedStatement stm = conn.prepareStatement
				("insert into Kund(namn, adress, postNr, ort, telefonNr, email) values(?, ?, ?, ?, ?, ?);", 
						ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				){
			stm.setString(1, namn);
			stm.setString(2, adress);
			stm.setInt(3, postNr);
			stm.setString(4, ort);
			stm.setString(5, telefonNr);
			stm.setString(6, email);
			stm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return getCostumerList();
	}

	public ObservableList<Customer> addSupplier(String namn, String adress, int postNr, String ort, String telefonNr, String email, int provisionsniva){
		try(
				PreparedStatement stm = conn.prepareStatement
				("insert into Kund(namn, adress, postNr, ort, telefonNr, email) values(?, ?, ?, ?, ?, ?);", 
						ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				PreparedStatement stm2 = conn.prepareStatement
						("insert into Leverantör(kundNr, provisionsnivå) values((select max(kund.kundNr) from Kund), ?);", 
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				){
			stm.setString(1, namn);
			stm.setString(2, adress);
			stm.setInt(3, postNr);
			stm.setString(4, ort);
			stm.setString(5, telefonNr);
			stm.setString(6, email);
			stm.executeUpdate();

			stm2.setInt(1, provisionsniva);
			stm2.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return getCostumerList();
	}

	public void addAuction(String kategori, String leverantornamn, String namn, String beskrivning, int utgangspris, int acceptpris, String startdatum, String slutdatum, boolean pagaende) {
		try(
				PreparedStatement stm = conn.prepareStatement
				("insert into Auktion(kategori, leverantör, namn, beskrivning, utgångspris, acceptpris, startdatum, slutdatum, pågående) values(?, ?, ?, ?, ?, ?, ?, ?, ?);", 
						ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				){
			stm.setString(1, kategori);
			stm.setString(2, leverantornamn);
			stm.setString(3, namn);
			stm.setString(4, beskrivning);
			stm.setInt(5, utgangspris);
			stm.setInt(6, acceptpris);
			stm.setString(7, startdatum);
			stm.setString(8, slutdatum);
			stm.setBoolean(9, pagaende);
			stm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ObservableList<Provision> getProvisionList(){
		ObservableList<Provision> provisionList = FXCollections.observableArrayList();
		try {
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select year(auktion.slutdatum) as years ,monthname(auktion.slutdatum) as månad, sum((select max(bud.summa) from bud where bud.auktion = auktion.auktionsNr and auktion.pågående = false)) as total, "
					+ "round(sum((select max(bud.summa) from bud where bud.auktion = auktion.auktionsNr)*((select provisionsnivå from leverantör where auktion.leverantör = leverantör.kundNr))/100)) as Provision from bud "
					+ "inner join auktion on auktion.auktionsNr = bud.auktion "
					+ "where summa >= all (select max(bud.summa) from bud where bud.auktion = auktion.auktionsNr and auktion.pågående = false) "
					+ "group by year(slutdatum), month(slutdatum) "
					+ "order by slutdatum;");
			while(rs.next()){
				provisionList.add(new Provision(
						rs.getInt("total"),
						rs.getInt("provision"),
						rs.getString("månad"),
						rs.getString("years")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
		return provisionList;
	}

	public boolean checkOngoing(ChronoLocalDate start, ChronoLocalDate slut){
		LocalDate date = LocalDate.now();
		if(date.compareTo(start) >= 0 && date.compareTo(slut) < 0){
			return true;
		}

		return false;
	}

	public ObservableList<Bid> getBidList(int auktionsNr){
		ObservableList<Bid> bidList = FXCollections.observableArrayList();
		ResultSet rs = null;
		try(
				PreparedStatement stm = conn.prepareStatement("SELECT * from Bud WHERE Bud.Auktion = ?", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

				) {
			stm.setInt(1, auktionsNr);
			rs = stm.executeQuery();
			while(rs.next()){
				bidList.add(new Bid(rs.getInt("Summa"), rs.getInt("Kund"), rs.getString("BudDatum"), rs.getString("Auktion")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return bidList;
	}


	public void updateBid(String customerNr, int sum, int auctionNr){

		try (
				PreparedStatement stm = conn.prepareStatement("INSERT INTO Bud (Auktion, Kund, Summa, BudDatum) VALUES (?, ?, ?, NOW())",ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				){

			stm.setInt(1, auctionNr);
			stm.setString(2, customerNr);
			stm.setInt(3, sum);
			stm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int getAuctionProvision(int auctionNr){
		int result = 0;
		ResultSet rs = null;
		try(

				PreparedStatement stm = conn.prepareStatement("SELECT round(max(bud.summa)*((select provisionsnivå from leverantör where auktion.leverantör = leverantör.kundNr)) /100) as Provision from auktion "
						+ "left join bud on bud.auktion = auktion.auktionsNr "
						+ "left join kund on kund.kundNr = bud.kund "
						+ "left join leverantör on leverantör.kundNr = kund.kundNr "
						+ "where auktion.auktionsNr = ?;",ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

				) {
			stm.setInt(1, auctionNr);
			rs = stm.executeQuery();
			while(rs.next()){
				result = rs.getInt("Provision");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return result;
	}
}

