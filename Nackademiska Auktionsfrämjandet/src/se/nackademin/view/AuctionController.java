package se.nackademin.view;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.controlsfx.dialog.Dialogs;

import se.nackademin.Main;
import se.nackademin.model.Auction;
import se.nackademin.model.Customer;
import se.nackademin.model.Provision;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

@SuppressWarnings("deprecation")
public class AuctionController {
	private String SQL = "select * from auktion";

	private Calendar c = new GregorianCalendar();
	private DatabaseController ac = new DatabaseController(this);
	@SuppressWarnings("unused")
	private Main main;
	private AnchorPane bidLayout;

	private ObservableList<Customer> customerList = FXCollections.observableArrayList();
	private ObservableList<Customer> customerListReport = FXCollections.observableArrayList();
	private ObservableList<String> auctions = FXCollections.observableArrayList();
	private ObservableList<Auction> auctionList = FXCollections.observableArrayList();
	private ObservableList<Provision> provisionListReport = FXCollections.observableArrayList();

	//Define ny profil
	@FXML private TextField profileNameField;	
	@FXML private TextField adressField;
	@FXML private TextField phoneNrField;
	@FXML private TextField areaField;
	@FXML private TextField zipCodeField;
	@FXML private TextField mailField;
	@FXML private TextField provisionField;

	@FXML private RadioButton customerRadio;
	@FXML private RadioButton supplierRadio;
	@FXML private Button btnAddProfile;

	@FXML private TableView<Customer> newProfileTable;
	@FXML private TableColumn<Customer, Number> customerCol;
	@FXML private TableColumn<Customer, String> nameCol;
	@FXML private TableColumn<Customer, String> addressCol;
	@FXML private TableColumn<Customer, String> phoneNrCol;
	@FXML private TableColumn<Customer, String> areaCol;
	@FXML private TableColumn<Customer, Number> zipCodeCol;
	@FXML private TableColumn<Customer, String> mailCol;
	@FXML private TableColumn<Customer, Number> provisionCol;

	//Define ny auktion
	@FXML private TextField auctionNameField;
	@FXML private TextField supplierField;
	@FXML private TextField startingPriceField;
	@FXML private TextField buyOutField;
	@FXML private ComboBox<String> chooseCategoryCombo;
	@FXML private DatePicker startDate1;
	@FXML private DatePicker endDate1;
	@FXML private TextArea description;
	@FXML private RadioButton buyOutPriceRadio;
	@FXML private Button btnAddAuction;

	//Define auktioner
	@FXML private ComboBox<String> chooseAuctionCombo;
	@FXML private ComboBox<String> chooseCategoryCombo_2;
	@FXML private RadioButton dateIntervalRadio;
	@FXML private RadioButton categoryRadio;
	@FXML private Button btnSearchAuction;
	@FXML private DatePicker startDate2;
	@FXML private DatePicker endDate2;

	@FXML private TableView<Auction> auctionTable;
	@FXML private TableColumn<Auction, Number> auctionNr;
	@FXML private TableColumn<Auction, String> category;
	@FXML private TableColumn<Auction, String> supplier;
	@FXML private TableColumn<Auction, String> name;
	@FXML private TableColumn<Auction, String> descriptionAuction;
	@FXML private TableColumn<Auction, Number> startingPrice;
	@FXML private TableColumn<Auction, Number> buyOutPrice;
	@FXML private TableColumn<Auction, String> startDateT;
	@FXML private TableColumn<Auction, String> endDateT;
	@FXML private TableColumn<Auction, String> ongoing;

	//define Rapport
	//Kundlista
	@FXML private TableView<Customer> profileTableR;
	@FXML private TableColumn<Customer, Number> customerColR;
	@FXML private TableColumn<Customer, String> nameColR;
	@FXML private TableColumn<Customer, String> addressColR;
	@FXML private TableColumn<Customer, String> phoneNrColR;
	@FXML private TableColumn<Customer, String> areaColR;
	@FXML private TableColumn<Customer, Number> zipCodeR;
	@FXML private TableColumn<Customer, String> mailColR;
	@FXML private TableColumn<Customer, Number> totalSumR;

	//Define Provision
	@FXML private TableView<Provision> provisionTableReport;
	@FXML private TableColumn<Provision, String> yearReport;
	@FXML private TableColumn<Provision, String> monthReport;
	@FXML private TableColumn<Provision, Number> totalReport;
	@FXML private TableColumn<Provision, Number> provisionReport;

	public void initialize(){
		setDate();
		auctions.add("P�g�ende");
		auctions.add("Ej P�g�ende");
		auctions.add("Samtliga");

		chooseCategoryCombo.setItems(ac.getCategory());
		chooseCategoryCombo.getSelectionModel().select(0);
		chooseAuctionCombo.setItems(auctions);
		chooseAuctionCombo.getSelectionModel().select(2);

		auctionTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if(event.getButton().equals(MouseButton.PRIMARY)){
					if(event.getClickCount() == 2){
						openBidDialog();
					}
				}
			}
		});
	}

	public void setMain(Main main){
		this.main = main;
		setCustomerTable();
		auctionList = ac.getAuctionList(SQL);
		setAuctionTable();
		setCustomerReportTable();
		setProvisionReportTable();
	}

	public void toggleCustomerSupplier(){
		if(supplierRadio.isSelected()){
			provisionField.setDisable(false);
		}
		else{
			provisionField.setDisable(true);
		}

		if(customerRadio.isSelected()){
			provisionField.setDisable(true);
		}
		else{
			provisionField.setDisable(false);
		}
	}

	public void toggleBuyOutPrice(){
		if(buyOutPriceRadio.isSelected())
			buyOutField.setDisable(false);
		else
			buyOutField.setDisable(true);
	}

	public void toggleCategory(){

		if(categoryRadio.isSelected()){
			chooseCategoryCombo_2.setDisable(false);
			dateIntervalRadio.setSelected(false);
			chooseCategoryCombo_2.setItems(ac.getCategory());
			chooseCategoryCombo_2.getSelectionModel().select(0);
			if(startDate2.isDisabled()){
			}
			else{
				startDate2.setDisable(true);
				endDate2.setDisable(true);
			}
		}
		else{
			chooseCategoryCombo_2.setDisable(true);
		}
	}

	public void toggleDateInterval(){
		if(dateIntervalRadio.isSelected()){
			startDate2.setDisable(false);
			endDate2.setDisable(false);
			categoryRadio.setSelected(false);
			startDate2.setValue(LocalDate.now());
			endDate2.setValue(LocalDate.now());
			if(chooseCategoryCombo_2.isDisabled()){
			}
			else{
				chooseCategoryCombo_2.setDisable(true);
			}
		}
		else{
			startDate2.setDisable(true);
			endDate2.setDisable(true);
			startDate2.setValue(null);
			endDate2.setValue(null);
		}
	}
	public void addProfile(){
		if(profileIsValid()){
			if(supplierRadio.isSelected()){
				ac.addSupplier(profileNameField.getText(), adressField.getText(), Integer.parseInt(zipCodeField.getText()), 
						areaField.getText(), phoneNrField.getText(), mailField.getText(), Integer.parseInt(provisionField.getText()));
			}
			else if (customerRadio.isSelected()){
				ac.addCustomer(profileNameField.getText(), adressField.getText(), Integer.parseInt(zipCodeField.getText()),
						areaField.getText(), phoneNrField.getText(), mailField.getText());
			}

			profileNameField.clear();
			adressField.clear();
			phoneNrField.clear();
			zipCodeField.clear();
			areaField.clear();
			mailField.clear();
			provisionField.clear();
			setCustomerTable();
		}
	}

	public boolean isSupplierSelected(){
		if(supplierRadio.isSelected()) return true;
		return false;
	}

	public void setCustomerTable(){
		customerList = ac.getCostumerList();

		customerCol.setCellValueFactory(cellData -> cellData.getValue().getCustomerNr());
		nameCol.setCellValueFactory(cellData -> cellData.getValue().getName());
		phoneNrCol.setCellValueFactory(cellData -> cellData.getValue().getPhoneNr());
		addressCol.setCellValueFactory(cellData -> cellData.getValue().getAddress());
		areaCol.setCellValueFactory(cellData -> cellData.getValue().getArea());
		zipCodeCol.setCellValueFactory(cellData -> cellData.getValue().getZipCode());
		mailCol.setCellValueFactory(cellData -> cellData.getValue().getMail());
		provisionCol.setCellValueFactory(cellData -> cellData.getValue().getProvisionLevel());

		newProfileTable.setItems(customerList);
	}

	public void setCustomerReportTable(){
		customerListReport = ac.getCustomerListTotal();

		customerColR.setCellValueFactory(cellData -> cellData.getValue().getCustomerNr());
		nameColR.setCellValueFactory(cellData -> cellData.getValue().getName());
		phoneNrColR.setCellValueFactory(cellData -> cellData.getValue().getPhoneNr());
		addressColR.setCellValueFactory(cellData -> cellData.getValue().getAddress());
		areaColR.setCellValueFactory(cellData -> cellData.getValue().getArea());
		zipCodeR.setCellValueFactory(cellData -> cellData.getValue().getZipCode());
		mailColR.setCellValueFactory(cellData -> cellData.getValue().getMail());
		totalSumR.setCellValueFactory(cellData -> cellData.getValue().getTotal());

		profileTableR.setItems(customerListReport);
	}

	public void setProvisionReportTable(){
		provisionListReport = ac.getProvisionList();

		yearReport.setCellValueFactory(cellData -> cellData.getValue().getYear());
		monthReport.setCellValueFactory(cellData -> cellData.getValue().getMonth());
		totalReport.setCellValueFactory(cellData -> cellData.getValue().getSalesTotal());
		provisionReport.setCellValueFactory(cellData -> cellData.getValue().getProvisionTotal());

		provisionTableReport.setItems(provisionListReport);
	}

	public void setAuctionTable(){

		auctionNr.setCellValueFactory(cellData -> cellData.getValue().getAuctionNr());
		category.setCellValueFactory(cellData -> cellData.getValue().getCategory());
		supplier.setCellValueFactory(cellData -> cellData.getValue().getSupplier());
		name.setCellValueFactory(cellData -> cellData.getValue().getName());
		descriptionAuction.setCellValueFactory(cellData -> cellData.getValue().getDescription());
		startingPrice.setCellValueFactory(cellData -> cellData.getValue().getStartingPrice());
		buyOutPrice.setCellValueFactory(cellData -> cellData.getValue().getBuyOut());
		startDateT.setCellValueFactory(cellData -> cellData.getValue().getStartDate());
		ongoing.setCellValueFactory(cellData -> cellData.getValue().getOngoing());
		endDateT.setCellValueFactory(cellData -> cellData.getValue().getEndDate());

		auctionTable.setItems(auctionList);
	}

	public void addAuction(){
		if(auctionIsValid()){
			if(buyOutPriceRadio.isSelected()){
				ac.addAuction(chooseCategoryCombo.getSelectionModel().getSelectedItem().toString(), supplierField.getText(),
						auctionNameField.getText(), description.getText(), Integer.parseInt(startingPriceField.getText()),
						Integer.parseInt(buyOutField.getText()), startDate1.getValue().toString(), 
						endDate1.getValue().toString(), ac.checkOngoing(startDate1.getValue(), endDate1.getValue()));
			} else if(!buyOutPriceRadio.isSelected()) {
				ac.addAuction(chooseCategoryCombo.getSelectionModel().getSelectedItem().toString(), supplierField.getText(),
						auctionNameField.getText(), description.getText(), Integer.parseInt(startingPriceField.getText()),
						0, startDate1.getValue().toString(), 
						endDate1.getValue().toString(), ac.checkOngoing(startDate1.getValue(), endDate1.getValue()));
			}

			supplierField.clear();
			auctionNameField.clear();
			description.clear();
			startingPriceField.clear();
			buyOutField.clear();
			setDate();
			auctionList = ac.getAuctionList(SQL);
			setAuctionTable();
		}
	}

	private boolean auctionIsValid() {
		String errorMessage = "";

		if (supplierField.getText() == null || supplierField.getText().length() == 0) {
			errorMessage += "Not a valid supplier!\n"; 
		}
		else{
			try{
				Integer.parseInt(supplierField.getText());
			}catch(NumberFormatException e){
				errorMessage += "Not a valid supplier!(must be an integer)\n"; 
			}
		}
		if (auctionNameField.getText() == null || auctionNameField.getText().length() == 0) {
			errorMessage += "Not a valid name!\n"; 
		}
		if (description.getText() == null || description.getText().length() == 0) {
			errorMessage += "Not a valid description!\n"; 
		}
		if (startingPriceField.getText() == null || startingPriceField.getText().length() == 0) {
			errorMessage += "Not a valid starting price!\n"; 
		} 
		else{
			try{
				Integer.parseInt(startingPriceField.getText());
			}catch(NumberFormatException e){
				errorMessage += "Not a valid starting price!(must be an integer)\n"; 
			}
		}
		if(buyOutPriceRadio.isSelected()){
			if (buyOutField.getText() == null || buyOutField.getText().length() == 0) {
				errorMessage += "Not a valid buy-out price!\n"; 
			} 
			else{
				try{
					Integer.parseInt(buyOutField.getText());
				}catch(NumberFormatException e){
					errorMessage += "Not a valid buy-out price!(must be an integer)\n"; 
				}
			}
		}

		if (errorMessage.length() == 0) {
			return true;
		} else {
			// Show the error message.
			Dialogs.create()
			.title("Invalid Fields")
			.masthead("Please correct invalid fields")
			.message(errorMessage)
			.showError();
			return false;
		}
	}
	
	private boolean profileIsValid() {
		String errorMessage = "";

		if (zipCodeField.getText() == null || zipCodeField.getText().length() == 0) {
			errorMessage += "Not a valid Zip Code!\n"; 
		}
		else{
			try{
				Integer.parseInt(zipCodeField.getText());
			}catch(NumberFormatException e){
				errorMessage += "Not a valid Zip Code!(must be an integer)\n"; 
			}
		}
		if (profileNameField.getText() == null || profileNameField.getText().length() == 0) {
			errorMessage += "Not a valid name!\n"; 
		}
		if (adressField.getText() == null || adressField.getText().length() == 0) {
			errorMessage += "Not an valid address!\n"; 
		}
		if (phoneNrField.getText() == null || phoneNrField.getText().length() == 0) {
			errorMessage += "Not a valid phone number!\n"; 
		} 
		if (areaField.getText() == null || areaField.getText().length() == 0) {
			errorMessage += "Not a valid area name!\n"; 
		} 
		if (mailField.getText() == null || mailField.getText().length() == 0) {
			errorMessage += "Not an valid E-mail!\n"; 
		} 
		
		if(supplierRadio.isSelected()){
			if (provisionField.getText() == null || provisionField.getText().length() == 0) {
				errorMessage += "Not a valid provision!\n"; 
			} 
			else{
				try{
					Integer.parseInt(provisionField.getText());
				}catch(NumberFormatException e){
					errorMessage += "Not a valid provision!(must be an integer)\n"; 
				}
			}
		}

		if (errorMessage.length() == 0) {
			return true;
		} else {
			// Show the error message.
			Dialogs.create()
			.title("Invalid Fields")
			.masthead("Please correct invalid fields")
			.message(errorMessage)
			.showError();
			return false;
		}
	}

	public void setDate(){
		startDate1.setValue(LocalDate.now());
		c.add(Calendar.DATE, 14);
		Date d = c.getTime();
		LocalDate date = d.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		endDate1.setValue(date);
	}

	public String checkAuctionForm(){
		String ongoing = "(true, false)";
		String startDate;
		String endDate;
		String category;
		String SQL = "select * from auktion";
		switch (chooseAuctionCombo.getSelectionModel().getSelectedIndex()){
		case 0:
			ongoing = "(true)";

			break;
		case 1:
			ongoing = "(false)";
			break;
		case 2:
			ongoing = "(true, false)";
			break;
		}

		SQL = "select * from auktion WHERE p�g�ende IN " + ongoing;

		if(dateIntervalRadio.isSelected()){
			startDate = startDate2.getValue().toString();
			endDate = endDate2.getValue().toString();
			SQL = "SELECT * FROM Auktion WHERE SlutDatum BETWEEN '" + startDate + "' AND '" + endDate + "' AND p�g�ende IN " + ongoing; 
		} else if (categoryRadio.isSelected()) {
			category = chooseCategoryCombo_2.getSelectionModel().getSelectedItem();
			SQL = "SELECT * FROM Auktion WHERE Kategori = '" + category + "' AND p�g�ende IN " + ongoing;

		}

		return SQL;
	}

	public void searchAuctionButtonIsClicked(){
		auctionList = ac.getAuctionList(checkAuctionForm());
		setAuctionTable();
	}

	private void openBidDialog() {
		Auction selectedAuktion = auctionTable.getSelectionModel().getSelectedItem();
		showBidDialog(selectedAuktion);
	}

	public void showBidDialog(Auction auktion){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/BudDialog.fxml"));
			bidLayout = (AnchorPane)loader.load();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("L�gg Till Bud/Historik");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.setResizable(false);
			Scene scene = new Scene(bidLayout);
			dialogStage.setScene(scene);
			scene.getStylesheets().add(Main.class.getResource("view/Styling2.css").toExternalForm());			
			BidController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setAuctionNr(auktion, this.ac);
			dialogStage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void updateCostumerTable(){
		setCustomerReportTable();
	}
	public void updateProvisionTable(){
		setProvisionReportTable();
	}
}


